import java.io.FileInputStream
import java.util.*

plugins {
    kotlin("jvm") version "1.8.21"
    id("java-library")
    `maven-publish`
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}

publishing {
    repositories {
        getLocalProperties()?.let { properties ->
            maven {
                name = "mobileSnapshot"
                url = uri("https://mobile.umbrellait.tech:8081/repository/mobile-snapshot/")
                credentials {
                    username = properties.getProperty("UMBRELLA_MOBILE_REPO_USER")
                    password = properties.getProperty("UMBRELLA_MOBILE_REPO_PASSWORD")
                }
            }
            maven {
                name = "mobileRelease"
                url = uri("https://mobile.umbrellait.tech:8081/repository/mobile-release/")
                credentials {
                    username = properties.getProperty("UMBRELLA_MOBILE_REPO_USER")
                    password = properties.getProperty("UMBRELLA_MOBILE_REPO_PASSWORD")
                }
            }
        }
    }
    publications {
        create<MavenPublication>("maven") {
            groupId = "com.umbrellait.mobile"
            artifactId = "module_inject"
            from(components["kotlin"])
        }
    }
}

repositories {
    mavenCentral()
}

fun getLocalProperties(): Properties? =
    file("local.properties").takeIf { it.exists() }?.let { propFile ->
        Properties().apply { load(FileInputStream(propFile)) }
    }